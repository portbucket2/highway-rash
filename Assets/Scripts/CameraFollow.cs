﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform mainCamera;
    
    public Transform target;
    
    public float smoothSpeed = 0.2f;
    public Vector3 offset;
    
    private void Start()
    {
        offset = mainCamera.position - target.position;
    }
    
    void LateUpdate ()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Slerp(mainCamera.position, desiredPosition, smoothSpeed*Time.deltaTime);
        mainCamera.position = smoothedPosition;
    }
}
