﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBike : MonoBehaviour
{
    public bool m_IsCarMove;
    [Range(0f,50f)]
    public float carMovementSpeed;
    public Rigidbody rigidbody;
    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        if (m_IsCarMove)
        {
            transform.position += new Vector3(0,0 ,carMovementSpeed * Time.deltaTime);
        }
    }

    public void StartCarMovment()
    {
        m_IsCarMove = true;
    }

    public void StopBike()
    {
        m_IsCarMove = false;
    }

    public void CreateExploison()
    {
        rigidbody.AddForce(Vector3.right * 30,ForceMode.Impulse);
        PlayExploision();
    }

    public void LeftExploision()
    {
        rigidbody.AddForce(Vector3.left * 30,ForceMode.Impulse);
        PlayExploision();
    }

    public void ResetPos(Vector3 t_Pos)
    {
        transform.position = t_Pos;
    }
    
    public void PlayExploision()
    {
        StartCoroutine(ExploisionRoutine());
    }

    IEnumerator ExploisionRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 1f;
        float t_EndTIme = Time.time + t_Duration;

        float t_CurrentRotX = transform.rotation.x;
        float t_CurrentRotY = transform.rotation.y;
        float t_CurrentRotZ = transform.rotation.z;
        float t_DestRotX = 360;
        float t_DestRotY = 360;
        float t_DestRotZ = 360;
        
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            transform.rotation = Quaternion.Euler(
                Mathf.Lerp(t_CurrentRotX,t_DestRotX,t_Progression),
                Mathf.Lerp(t_CurrentRotY,t_DestRotY,t_Progression),
                Mathf.Lerp(t_CurrentRotZ,t_DestRotZ,t_Progression)
            );
            
            if (t_Progression >= 1f)
            {
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(ExploisionRoutine());
    }
}
