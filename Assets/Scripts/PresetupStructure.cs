﻿using UnityEngine;


[System.Serializable]
public struct NPCStruct
{
    public Transform instancePosition;
    public GameObject instancePrefab;
}