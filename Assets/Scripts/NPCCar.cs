﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCar : MonoBehaviour
{
    public Transform carTrans;
    
    public Transform destTrans;
    public ParticleSystem exploisionParticle;
    public Rigidbody rigidbody;
    private bool m_PlayerReachedCarBarrier;
    
    public bool m_IsCarMove;
    [Range(0f,50f)]
    public float carMovementSpeed;
    

    private Coroutine m_MovementRoutine;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }


    private void Start()
    {
        //PlayExploision();
    }

    private void Update()
    {
        if (m_IsCarMove)
        {
            transform.position += new Vector3(0,0 ,carMovementSpeed * Time.deltaTime);
        }
    }

    public void StartCarMovment()
    {
        m_IsCarMove = true;
    }

    public void StopCar()
    {
        m_IsCarMove = false;
    }

    public void ResetPos(Vector3 t_Pos)
    {
        transform.position = t_Pos;
    }

    public void PlayerComeAtBarrier(Transform otherTransform)
    {
        m_PlayerReachedCarBarrier = true;
    }

    public void StopMovementRoutine()
    {
        StopCoroutine(m_MovementRoutine);
    }
    public void CreateExploison()
    {
        rigidbody.AddForce(Vector3.left * 30,ForceMode.Impulse);
        PlayExploision();
    }

    public void RightCreateExploision()
    {
        rigidbody.AddForce(Vector3.right * 30,ForceMode.Impulse);
        PlayExploision();
    }

    public void PlayExploision()
    {
        StartCoroutine(ExploisionRoutine());
    }

    IEnumerator ExploisionRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 1f;
        float t_EndTIme = Time.time + t_Duration;

        float t_CurrentRotX = carTrans.rotation.x;
        float t_CurrentRotY = carTrans.rotation.y;
        float t_CurrentRotZ = carTrans.rotation.z;
        float t_DestRotX = 360;
        float t_DestRotY = 360;
        float t_DestRotZ = 360;
        
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            carTrans.rotation = Quaternion.Euler(
                Mathf.Lerp(t_CurrentRotX,t_DestRotX,t_Progression),
                Mathf.Lerp(t_CurrentRotY,t_DestRotY,t_Progression),
                Mathf.Lerp(t_CurrentRotZ,t_DestRotZ,t_Progression)
                );
            
            if (t_Progression >= 1f)
            {
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(ExploisionRoutine());
    }
    
}
