﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldTrigger : MonoBehaviour
{
   public NPCOld npcOld;

   private void OnTriggerEnter(Collider other)
   {
      if (other.CompareTag("Player"))
      {
         npcOld.Triggered();
      }
   }
}
