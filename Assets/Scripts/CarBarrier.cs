﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBarrier : MonoBehaviour
{
    public List<NPCCar> npcCarList;
    
    
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {
            for (int i = 0; i < npcCarList.Count; i++)
            {
                npcCarList[i].PlayerComeAtBarrier(other.transform);
            }

            PlayerController.Instance.IncreaseFactors();
            
        }
    }
}
