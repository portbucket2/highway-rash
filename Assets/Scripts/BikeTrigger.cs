﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikeTrigger : MonoBehaviour
{

    public KickSide kickSide;
    
     private void OnTriggerEnter(Collider other)
     {
    
    if (other.CompareTag("NPCCharacter"))
    {
        PlayerController.Instance.Triggerred(kickSide, other.transform);
    }
    
    if (other.CompareTag("NPCCar"))
    {
        PlayerController.Instance.TriggeredNPCCar(kickSide, other.transform);
    }
    
    if (other.CompareTag("NPCBike"))
    {
        PlayerController.Instance.TriggeredNPCBike(kickSide, other.transform);
    }
    
    if (other.CompareTag("NPCBoss"))
    {
        PlayerController.Instance.TriggeredNPCBoss(kickSide, other.transform);
    }
    
    if (other.CompareTag("LevelComplete"))
    {
        PlayerController.Instance.LevelComplete();
    }

    if (other.CompareTag("NPCOld"))
    {
        PlayerController.Instance.PlayerJump(other.transform);
    }
     }
     
}


