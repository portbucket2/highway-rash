﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCharacter : MonoBehaviour
{
    [Range(0f, 20f)] public float movementSpeed;
    
    public Animator animator;
    public BoxCollider boxCollider;
    
    
    private int IDLE = Animator.StringToHash("idle");
    private int WALK = Animator.StringToHash("walk");
    private int RUN = Animator.StringToHash("run");
    private int KICK = Animator.StringToHash("kick");
    
    
    
    private bool m_IsCheck;

    public bool m_IsRoadsideCharacter;

    private Vector3 m_InitPos;
    
    
    
    public Transform playerTrans;

    public List<Transform> focusingTransform;
    
    
    public void PlayIdle()
    {
        animator.SetTrigger(IDLE);
    }

    public void PlayWalk()
    {
        animator.SetTrigger(WALK);
    }

    public void PlayRun()
    {
        animator.SetTrigger(RUN);
    }

    public void PlayKick()
    {
        animator.SetTrigger(KICK);
    }


    public void PlayerComeAtBarrier(Transform t_PlayerTrans)
    {
        PlayRun();
        m_IsCheck = true;
    }

    public void StartRoadSideMovement()
    {
        PlayWalk();
        m_IsRoadsideCharacter = true;
    }

    public void MakeCharacterIdle()
    {
        m_IsCheck = false;
        boxCollider.enabled = false;
        PlayIdle();
    }

    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    private void Start()
    {
        m_InitPos = transform.position;
    }

    private void Update()
    {
        if (m_IsCheck)
        {
            //transform.LookAt(playerTrans);
            transform.position += Vector3.forward * movementSpeed * Time.deltaTime;
            
            //Debug.Log(Vector3.SignedAngle(transform.position,playerTrans.position,Vector3.forward));

            // if (Vector3.Distance(transform.position, playerTrans.position) < 12f)
            // {
            //     PlayKick();
            // }
        }

        if (m_IsRoadsideCharacter)
        {
            transform.position += Vector3.forward * movementSpeed * Time.deltaTime;
        }
    }

    public void ResetInitPos()
    {
        m_IsCheck = false;
        boxCollider.enabled = true;
        animator.enabled = true;
        PlayIdle();
        transform.position = m_InitPos;
    }
}
