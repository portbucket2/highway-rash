﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentCrossLine : MonoBehaviour
{
    public LevelEnv levelEnv;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            levelEnv.SegmentCrossed();
        }
    }
}
