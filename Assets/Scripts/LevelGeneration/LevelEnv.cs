﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class LevelEnv : MonoBehaviour
{
    public GameObject prefabGo;
    public int id;
    public bool isIdle;

    public GameObject humanSpawnGo;
    public List<NPCStruct> carSpawnStruct;
    public List<NPCStruct> bikeSpawnStruct;
    public NPCStruct bossSpawnStruct;
    public NPCStruct levelCompleteStuct;
    
    
    
    public GameObject carObject;
    public GameObject bikeObject;
    public GameObject bossObject;
    public GameObject levelCompleteObject;

    [Header("Old Man Segment")] public GameObject oldManSegment;

    [Header("Spawn Segment")] public List<NPCCharacter> npcCharacterList;

    private void Awake()
    {
        prefabGo = this.gameObject;
    }

    public void SpawnHuman()
    {
        humanSpawnGo.SetActive(true);
    }

    public void SpawnLevelComplete()
    {
        levelCompleteStuct.instancePrefab.SetActive(true);
    }

    public void SpawnBoss()
    {
        // bossObject = Instantiate(bossSpawnStruct.instancePrefab,bossSpawnStruct.instancePosition.position,
        //         new Quaternion(0,-180,0,0),carSpawnStruct.instancePosition);
        //
        // bossObject.GetComponent<NPCBoss>().StartCarMovment();
    }

    public void SpawnCar()
    {
        int t_RandValue = UnityEngine.Random.Range(0, 100);
        if (t_RandValue < 50)
        {
            carObject = Instantiate(carSpawnStruct[0].instancePrefab, carSpawnStruct[0].instancePosition.position,
                new Quaternion(0,180,0,0),carSpawnStruct[0].instancePosition);
            carObject.GetComponent<NPCCar>().StartCarMovment();
        }
        else
        {
            carObject = Instantiate(carSpawnStruct[1].instancePrefab, carSpawnStruct[1].instancePosition.position,
                new Quaternion(0,180,0,0),carSpawnStruct[1].instancePosition);
            carObject.GetComponent<NPCCar>().StartCarMovment();
        }
    }

    
    
    public void SpawnBike()
    {
        int t_RandValue = UnityEngine.Random.Range(0, 100);
        if (t_RandValue < 50)
        {
            bikeObject = Instantiate(bikeSpawnStruct[0].instancePrefab, bikeSpawnStruct[0].instancePosition.position,
                Quaternion.identity, bikeSpawnStruct[0].instancePosition);
            bikeObject.GetComponent<NPCBike>().StartCarMovment();
        }
        else
        {
            bikeObject = Instantiate(bikeSpawnStruct[1].instancePrefab, bikeSpawnStruct[1].instancePosition.position,
                Quaternion.identity, bikeSpawnStruct[1].instancePosition);
            bikeObject.GetComponent<NPCBike>().StartCarMovment();
        }
    }

    public void ResetHuman()
    {
        for (int i = 0; i < npcCharacterList.Count; i++)
        {
            npcCharacterList[i].ResetInitPos();
        }
        humanSpawnGo.SetActive(false);
    }

    public void ResetCar()
    {
        if (carObject != null)
        {
            Destroy(carObject);
            //carObject.GetComponent<NPCCar>().StopCar();
            //carObject.GetComponent<NPCCar>().ResetPos(carSpawnStruct.instancePosition.position);
        }
    }

    public void ResetBike()
    {
        if (bikeObject != null)
        {
            Destroy(bikeObject);
            //bikeObject.GetComponent<NPCBike>().StopBike();
            //bikeObject.GetComponent<NPCBike>().ResetPos(bikeSpawnStruct.instancePosition.position);
        }
    }


    public Quaternion GetRotation()
    {
        return new Quaternion(transform.rotation.x,transform.rotation.y,transform.rotation.z,transform.rotation.w);
    }

    public void SegmentCrossed()
    {
        LevelGenManager.Instance.SegmentCrossed(id);
    }

    public void SetPosition(Vector3 t_IdlePosition)
    {
        transform.position = t_IdlePosition;
    }

    public void SetIdle(bool t_IsIdle)
    {
        isIdle = t_IsIdle;
    }

    public void ActivateLevel()
    {
        gameObject.SetActive(true);
    }

    public void DeactivateLevel()
    {
        gameObject.SetActive(false);
    }

    public void SegmentEntry()
    {
        LevelGenManager.Instance.SegmentEntry(this);
    }

    public void SpawnOld()
    {
        oldManSegment.SetActive(true);
        oldManSegment.transform.GetChild(3).gameObject.SetActive(true);
        oldManSegment.transform.GetChild(3).GetComponent<NPCOld>().enabled = true;
        oldManSegment.transform.GetChild(3).GetComponent<NPCOld>().ResetAll();
    }

    public void SpawnOffEntry()
    {
        humanSpawnGo.SetActive(true);
        for (int i = 0; i < npcCharacterList.Count; i++)
        {
            npcCharacterList[i].StartRoadSideMovement();
        }
      
    }
}
