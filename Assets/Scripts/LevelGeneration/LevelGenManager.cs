﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenManager : MonoBehaviour
{
   public List<LevelEnv> levelEnvGameobjectList;

   public List<LevelEnv> levelEnvInsList;

   public Vector3 startPosition;

   public Vector3 idlePosition;
   [Range(0f, 100f)] public float segmentDistance;
   public int currentSegmentNumber = 0;
   public int currentCrossedSegmentNumber = 0;
   public List<int> crossedSegmentIdList;

   private bool m_IsBossSpawn;
   private bool m_IsLevelCompleteSpawn;
   
   public static LevelGenManager Instance;

   private void Awake()
   {
      if (Instance == null)
      {
         Instance = this;
      }
      
      crossedSegmentIdList = new List<int>();
   }


   private void Start()
   {
      Initialize();
      SetToStart();
   }

   private void SetToStart()
   {
      for (int i = 0; i < 4; i++)
      {
         Vector3 t_ModifiedPos = startPosition +  new Vector3(0,0,i*segmentDistance);
         levelEnvInsList[i].SetPosition(t_ModifiedPos);
         levelEnvInsList[i].SetIdle(false);
         levelEnvInsList[i].ActivateLevel();
         
         if (i == 0)
         {
            levelEnvInsList[i].SpawnOffEntry();
         }
      }

      currentSegmentNumber = 4;
   }


   public void Initialize()
   {
      levelEnvInsList = new List<LevelEnv>();
      int t_Counter = 0;
      
      for (int i = 0; i < levelEnvGameobjectList.Count; i++)
      {
         for (int j = 0; j < 10; j++)
         {
            GameObject t_EnvGameobject = Instantiate(levelEnvGameobjectList[i].prefabGo,idlePosition,levelEnvGameobjectList[i].GetRotation(),transform);
            LevelEnv t_LevelEnv = t_EnvGameobject.GetComponent<LevelEnv>();
            t_LevelEnv.id = t_Counter;
            t_LevelEnv.isIdle = true;
            
            t_LevelEnv.DeactivateLevel();
            
            levelEnvInsList.Add(t_EnvGameobject.GetComponent<LevelEnv>());
            t_Counter++;
         }
      }
      Shuffle(levelEnvInsList);
   }
   
   void Shuffle (List<LevelEnv> list)
   {
      for (int i = 0; i < list.Count; i++) 
      {
         int randomIndex = UnityEngine.Random.Range (i, list.Count);
         LevelEnv temp = list [i];
         list [i] = list [randomIndex];
         list [randomIndex] = temp;
      }
   }

   public void SegmentCrossed(int t_CrossedId)
   {
      currentCrossedSegmentNumber++;
      
      Debug.Log(currentCrossedSegmentNumber);
      crossedSegmentIdList.Add(t_CrossedId);
      
      
      if (currentCrossedSegmentNumber % 2 == 0)
      {
         //add from idle
         int t_AddCounter = 0;
         for (int i = 0; i < levelEnvInsList.Count; i++)
         {
            if (levelEnvInsList[i].isIdle)
            {
               Vector3 t_ModifiedPos = startPosition +  new Vector3(0,0,currentSegmentNumber * segmentDistance);
               levelEnvInsList[i].SetPosition(t_ModifiedPos);
               levelEnvInsList[i].SetIdle(false);
               levelEnvInsList[i].ActivateLevel();
               
               currentSegmentNumber++;
               t_AddCounter++;

               if (t_AddCounter == 2)
               {
                  break;
               }
            }
         }
         
         
         //remove and set to idle
         for (int i = 0; i < levelEnvInsList.Count; i++)
         {
            if (levelEnvInsList[i].id == crossedSegmentIdList[0])
            {
               levelEnvInsList[i].SetPosition(idlePosition);
               levelEnvInsList[i].SetIdle(true);
               
               levelEnvInsList[i].ResetHuman();
               levelEnvInsList[i].ResetBike();
               levelEnvInsList[i].ResetCar();
               levelEnvInsList[i].SpawnOld();
               
               levelEnvInsList[i].DeactivateLevel();
               
            }
         }
         
         crossedSegmentIdList.RemoveAt(0);
      }
   }

   public void SegmentEntry(LevelEnv t_LevelEnv)
   {
     // t_LevelEnv.SpawnHuman();


      if (currentSegmentNumber % 4 == 0)
      {
         t_LevelEnv.SpawnOld();
      }
      
      
      if (currentSegmentNumber <= 8)
      {
         //t_LevelEnv.SpawnHuman();
      }
      else if (currentSegmentNumber > 8 && currentSegmentNumber <= 15)
      {
         t_LevelEnv.SpawnBike();
      }
      else if (currentSegmentNumber > 15 && currentSegmentNumber <= 20)
      {
         t_LevelEnv.SpawnCar();
      }
      else if (currentSegmentNumber > 20 && currentSegmentNumber <= 26)
      {
         t_LevelEnv.SpawnBike();
         t_LevelEnv.SpawnCar();
      }
      else
      {
         if (!m_IsLevelCompleteSpawn)
         {
            t_LevelEnv.SpawnLevelComplete();
            m_IsLevelCompleteSpawn = true;
         }
      }
   }
}


