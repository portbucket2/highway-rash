﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOffEntry : MonoBehaviour
{
    public LevelEnv levelEnv;
    
    private void OnTriggerEnter(Collider other)
    {
       
        if (other.CompareTag("Player"))
        {
            
            levelEnv.SpawnOffEntry();
        }
    }
}
