﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScrolingController : MonoBehaviour
{
    #region Custom Variables

    public enum Direction
    {
        left,
        right,
        down,
        up,
        back,
        forward
    };

    #endregion

    #region Public Variables

    [Range(2,100)]
    public int stageLength;
    [Range(0f,100f)]
    public float defaultStageMovementSpeed;
    public Direction stageDirection;
    public List<Transform> stageTiles;

    #endregion

    #region Private Variables

    private bool m_IsMovementControllerRunning;

    private Transform m_TransformReference;

    private Vector3[] m_InitialPosition;

    private Vector3 m_ResetPosition;
    private Vector3 m_SpawnPosition;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_TransformReference = transform;
    }

    private void Start()
    {
        PreProcess();
    }

    #endregion

    #region Configuretion

    private Vector3 GetDirection() {

        Vector3 t_Result = Vector3.zero;

        switch (stageDirection) {
            case Direction.left:
                t_Result = Vector3.left;
                break;
            case Direction.right:
                t_Result = Vector3.right;
                break;
            case Direction.down:
                t_Result = Vector3.down;
                break;
            case Direction.up:
                t_Result = Vector3.up;
                break;
            case Direction.back:
                t_Result = Vector3.back;
                break;
            case Direction.forward:
                t_Result = Vector3.forward;
                break;

        }

        return t_Result;
    }

    private Vector3 GetStageTilePosition(int t_TileIndex) {

        return m_TransformReference.position + (t_TileIndex * (GetDirection() * 50f));
    }

    private IEnumerator ControllerForMovingStageTile() {

        WaitForSeconds t_CycleDelay = new WaitForSeconds(0.0167f);

        bool t_SnapAllPosition = false;
        
        int t_NumberOfTileToMove = stageTiles.Count;

        float t_AbsoluteSpeedOfStageMovement;
        float t_Distance;

        Vector3 t_ModifiedPosition;

        yield return new WaitForSeconds(1f);

        while (m_IsMovementControllerRunning) {

            t_AbsoluteSpeedOfStageMovement = defaultStageMovementSpeed * Time.deltaTime;

            for (int tileIndex = 0; tileIndex < t_NumberOfTileToMove; tileIndex++) {

                t_ModifiedPosition = Vector3.MoveTowards(
                        stageTiles[tileIndex].position,
                        m_ResetPosition,
                        t_AbsoluteSpeedOfStageMovement
                    );

                t_Distance = Vector3.Distance(t_ModifiedPosition, m_ResetPosition);

                if (t_Distance == 0)
                {
                    stageTiles[tileIndex].position = m_SpawnPosition;
                    t_SnapAllPosition = true;
                }
                else if (t_Distance > 0 && t_Distance <= 0.1f)
                {

                    stageTiles[tileIndex].position = m_ResetPosition;
                }
                else {
                    stageTiles[tileIndex].position = t_ModifiedPosition;
                }
            }

            if (t_SnapAllPosition) {

                float t_CalculatedDistance;
                float t_MinDistance;
                Vector3 t_SnappedPosition = Vector3.zero;

                for (int tileIndex = 0; tileIndex < t_NumberOfTileToMove; tileIndex++) {

                    t_MinDistance = Mathf.Infinity;

                    for (int initialPosIndex = 0; initialPosIndex < t_NumberOfTileToMove; initialPosIndex++) {

                        t_CalculatedDistance = Vector3.Distance(stageTiles[tileIndex].position, m_InitialPosition[initialPosIndex]);
                        if (t_CalculatedDistance < t_MinDistance) {

                            t_MinDistance = t_CalculatedDistance;
                            t_SnappedPosition = m_InitialPosition[initialPosIndex];
                        }
                    }

                    stageTiles[tileIndex].position = t_SnappedPosition;
                }

                t_SnapAllPosition = false;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForMovingStageTile());
    }

    #endregion

    #region Public Callback

    public void PreProcess() {

        List<Transform> m_TemporaryStageTiles = new List<Transform>(stageTiles);

        Vector3 m_ModifiedPosition = m_TransformReference.position;
        m_ResetPosition = GetStageTilePosition(-1);

        int t_IndexOfSelectedStageTile;
        int t_NumberOfStageTiles = m_TemporaryStageTiles.Count;

        int t_IndexOfInitialPosition = 0;
        m_InitialPosition = new Vector3[t_NumberOfStageTiles];

        while (t_NumberOfStageTiles > 0) {

            t_IndexOfSelectedStageTile = Random.Range(0, m_TemporaryStageTiles.Count);

            m_TemporaryStageTiles[t_IndexOfSelectedStageTile].position = GetStageTilePosition( stageTiles.Count - t_NumberOfStageTiles);

            m_InitialPosition[t_IndexOfInitialPosition++] = m_TemporaryStageTiles[t_IndexOfSelectedStageTile].position;

            if (t_NumberOfStageTiles == stageTiles.Count)
                m_SpawnPosition = m_TemporaryStageTiles[t_IndexOfSelectedStageTile].position;
            else if (t_NumberOfStageTiles == 1)
                m_ResetPosition = GetStageTilePosition(stageTiles.Count);

            m_TemporaryStageTiles.RemoveAt(t_IndexOfSelectedStageTile);
            m_TemporaryStageTiles.TrimExcess();

            t_NumberOfStageTiles--;
        }

        m_IsMovementControllerRunning = true;
        StartCoroutine(ControllerForMovingStageTile());
    }

    #endregion
}
