﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using com.faithstudio.SDK;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Start Panel")] public TextMeshProUGUI levelNumberText;
    public Button tapButton;
    public Animator startPanelAnimator;
    
    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private static UIManager m_Instance;
    
    [Header("Upper panel")] public Animator upperPanelAnimator;
    public TextMeshProUGUI currentLevelText;
    
    
    [Header("Level Complete")] public Animator levelCompleteAnimator;
    public Button collectButton;
    public ParticleSystem levelCompleteParticle;

    public GameObject dymanicObject;
    

    public static UIManager Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
    }

    private void Start()
    {
        currentLevelText.text = LevelManager.Instance.GetCurrentLevelWithLevelText();
        levelNumberText.text = LevelManager.Instance.GetCurrentLevelWithLevelText();
        ButtonInteraction();
    }

    public void ActivateDynamicObject()
    {
        dymanicObject.SetActive(true);
    }

    public void DeactivateDynamicObject()
    {
        dymanicObject.SetActive(false);
    }

    private void ButtonInteraction()
    {
       tapButton.onClick.AddListener(delegate
       {
           startPanelAnimator.SetTrigger(EXIT);
           upperPanelAnimator.SetTrigger(ENTRY);
           FacebookAnalyticsManager.Instance.FBALevelStart(LevelManager.Instance.GetCurrentLevel());
           PlayerController.Instance.StartGame();
       });
    }
    
    public void ShowLevelComplete()
    {
        DeactivateDynamicObject();
        
        levelCompleteAnimator.SetTrigger(ENTRY);
        
        levelCompleteParticle.Play();
        
        FacebookAnalyticsManager.Instance.FBALevelComplete(LevelManager.Instance.GetCurrentLevel());
        
        collectButton.onClick.AddListener(delegate
        {
            SceneManager.LoadScene("Scenes/Road-Rash-Gameplay");
        });
    }
}
