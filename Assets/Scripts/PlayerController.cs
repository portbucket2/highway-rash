﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour
{
    public Transform playerTrans;
    public Transform playerBodyTrans;
    
    [Range(0f, 100f)] public float movementSpeed;

    public Transform mainCamera;
    
    public float smoothSpeed = 0.2f;
    public Vector3 offset;

    public Animator playerAnimator;
    private int LEFT_KICK = Animator.StringToHash("leftkick");
    private int RIGHT_KICK = Animator.StringToHash("rightkick");
    private int BIKE_IDLE = Animator.StringToHash("bikeidle");
    private int RIGHT_HIT = Animator.StringToHash("righthit");
    private int LEFT_HIT = Animator.StringToHash("lefthit");
    
    private KickSide kickSide;
    private Transform m_CurrentTriggeredTrans;

    private bool m_ChaseCar;
    private bool m_IsHitCar;
    private Transform m_HitCarTrans;
    
    public NPCCar npcCar;
    public Transform finalCameraTrans;

    [Header("Particles")] public ParticleSystem leftHitParticle;
    public ParticleSystem rightHitParticle;
    public ParticleSystem speedParticle;
    public bool m_IsSpeedBoost;
    private float m_SpeedBoostTimer;
    private bool m_IsGameRunning;

    [Header("Baton")] public GameObject rightBaton;
    public Transform rightBatonParentTrans;
    public Transform rightBatonTrans;
    private Vector3 m_RightBattonInitPos;
    private Quaternion m_RightBattonInitRot;
    
    public GameObject leftBaton;
    public Transform leftBatonParentTrans;
    public Transform leftBatonTrans;
    private Vector3 m_LeftBattonInitPos;
    private Quaternion m_LeftBattonInitRot;
    
    

    [Header("Speed Variables")]
    public float maxSpeed;

    public Transform originTrans;
    public List<Transform> targetOriginTrans;
    
    public static PlayerController Instance;
    
    private UnityAction m_CameraReachedOnLast;
    private Vector3 m_PlayerCamDefaultPos;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        
    }

    private void Start()
    {
        m_RightBattonInitPos = rightBaton.transform.localPosition;
        m_RightBattonInitRot = rightBaton.transform.localRotation;

        m_LeftBattonInitPos = leftBaton.transform.localPosition;
        m_LeftBattonInitRot = leftBaton.transform.localRotation;
        
        m_PlayerCamDefaultPos = CameraMovementController.Instance.defaultCameraPositionOffset;
        
        m_CameraReachedOnLast += CameraReachedOnLast;
    }

    public void StartGame()
    {
        StartCoroutine(StartGameRoutine());
    }

    private IEnumerator StartGameRoutine()
    {
        CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
        DragController.Instance.StartGame();
        DragController.Instance.DragDirection = DragDirection.HORIZONTAL;
        offset = mainCamera.position - playerTrans.position;
        //DragController.Instance.buttonUpDelegate += ButtonUpCalled;
        
        yield return new WaitForSeconds(0.5f);
        movementSpeed = 25f;
        PlayBikeIdle();
        m_IsGameRunning = true;
        m_IsSpeedBoost = false;
        UIManager.Instance.ActivateDynamicObject();
    }

   


    private void Update()
    {
        if (m_IsGameRunning)
        {
            MoveObject(DragController.Instance.MoveDirection);
            RotatePlayer(DragController.Instance.RotationValue);

            if (m_ChaseCar)
            {
                // if (Vector3.Distance(npcCar.transform.position, transform.position) > 8)
                // {
                //     movementSpeed -= 1f;
                // }
            }

            if (m_IsSpeedBoost)
            {
                m_SpeedBoostTimer += Time.deltaTime;
                if (!IsMaxSpeed())
                {
                    movementSpeed += Time.deltaTime * 10f;
                }

                if (m_SpeedBoostTimer > 0.5f)
                {
                    if (speedParticle.isPlaying)
                    {
                        speedParticle.Stop();
                    }

                    m_IsSpeedBoost = false;
                    m_SpeedBoostTimer = 0;
                }
            }
            else
            {
                movementSpeed -= Time.deltaTime * 2;
                if (movementSpeed < 25)
                {
                    movementSpeed = 25;
                }
            }
        }

    }

    private Coroutine left;
    private Coroutine right;
    private bool isRunning;
    

    public void HitOrKick(KickSide t_KickSide,Transform t_Transform)
    {
        if (t_KickSide == KickSide.LEFT)
        {
            int t_RandValue = Random.Range(0, 100);

            if (t_RandValue < 50)
            {
                //kick
                StartCoroutine(PlayerLeftKickRoutine(t_Transform));
            }
            else
            {
                //batton
                StartCoroutine(LeftCharacterHitRoutine(t_Transform));
            }
        } 
        else if (t_KickSide == KickSide.RIGHT)
        {
            int t_RandValue = Random.Range(0, 100);
            if (t_RandValue < 50)
            {
                StartCoroutine(PlayerRightKickRoutine(t_Transform));
            }
            else
            {
                StartCoroutine(RightCharacterHitRoutine(t_Transform));
            }
        }
    }
    

    
    
    private IEnumerator HitCarRoutine()
    {
        m_HitCarTrans.GetComponent<NPCCar>().StopMovementRoutine();
        m_HitCarTrans.GetComponent<NPCCar>().PlayExploision();
        yield return new WaitForEndOfFrame();
        
        movementSpeed = 0;
        yield return new WaitForSeconds(.5f);
        //UIManager.Instance.ShowLevelComplete();
    }

    public void LevelComplete()
    {
        LevelManager.Instance.IncreaseGameLevel();
        StartCoroutine(LevelCompleteRoutine());
    }

    private IEnumerator LevelCompleteRoutine()
    {
        yield return new WaitForEndOfFrame();
        movementSpeed = 0;
        m_IsGameRunning = false;
        yield return new WaitForSeconds(.5f);
        UIManager.Instance.ShowLevelComplete();
    }

    private void CameraReachedOnLast()
    {
       
    }
    
    IEnumerator RightCharacterHitRoutine(Transform t_Transform)
    {
        playerAnimator.SetTrigger(RIGHT_HIT);
        yield return new WaitForSeconds(0.15f);
        
        rightBaton.transform.SetParent(rightBatonTrans);
        rightBaton.transform.localPosition = Vector3.zero;
        rightBaton.transform.localRotation = Quaternion.Euler(new Vector3(0,-225,0));
        
       // if (m_CurrentTriggeredTrans != null)
        {
            leftHitParticle.Play();

            yield return new WaitForEndOfFrame();
            t_Transform.GetComponent<NPCCharacter>().MakeCharacterIdle();
            t_Transform.GetChild(0).GetComponent<Animator>().enabled = false;
            yield return new WaitForEndOfFrame();
           
            
            m_IsSpeedBoost = true;
            speedParticle.Play();

            CameraMovementController.Instance.defaultForwardVelocity -= .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
            
            //yield return new WaitForSeconds(.2f);
            //m_CurrentTriggeredTrans = null;
            
            yield return new WaitForSeconds(.3f);
            CameraMovementController.Instance.defaultForwardVelocity += .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
        }
        rightBaton.transform.SetParent(rightBatonParentTrans);
        rightBaton.transform.localPosition = m_RightBattonInitPos;
        rightBaton.transform.localRotation = ( m_RightBattonInitRot);
    }

    private IEnumerator LeftCharacterHitRoutine(Transform t_Transform)
    {
        playerAnimator.SetTrigger(LEFT_HIT);
        yield return new WaitForSeconds(0.15f);
        
        leftBaton.transform.SetParent(leftBatonTrans);
        leftBaton.transform.localPosition = Vector3.zero;
        leftBaton.transform.localRotation = Quaternion.Euler(new Vector3(0,270,-110));
        
       // if (m_CurrentTriggeredTrans != null)
        {
            //yield return new WaitForSeconds(.2f);
            leftHitParticle.Play();

            
            t_Transform.GetComponent<NPCCharacter>().MakeCharacterIdle();
            t_Transform.GetChild(0).GetComponent<Animator>().enabled = false;
            yield return new WaitForEndOfFrame();
           
            
            m_IsSpeedBoost = true;
            speedParticle.Play();

            CameraMovementController.Instance.defaultForwardVelocity -= .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
            
            
           // m_CurrentTriggeredTrans = null;
            
            yield return new WaitForSeconds(.3f);
            CameraMovementController.Instance.defaultForwardVelocity += .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
        }
        
        leftBaton.transform.SetParent(leftBatonParentTrans);
        leftBaton.transform.localPosition = m_LeftBattonInitPos;
        leftBaton.transform.localRotation = ( m_LeftBattonInitRot);
     
    }


    IEnumerator PlayerLeftKickRoutine(Transform t_Transform)
    {
        //if (m_CurrentTriggeredTrans != null)
        {
            playerAnimator.SetTrigger(LEFT_KICK);

            yield return new WaitForSeconds(.25f);
            leftHitParticle.Play();

            yield return new WaitForEndOfFrame();
            t_Transform.GetComponent<NPCCharacter>().MakeCharacterIdle();
            t_Transform.GetChild(0).GetComponent<Animator>().enabled = false;
            yield return new WaitForEndOfFrame();
           

            
            m_IsSpeedBoost = true;
            speedParticle.Play();

            CameraMovementController.Instance.defaultForwardVelocity -= .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
            
            yield return new WaitForSeconds(.2f);
            m_CurrentTriggeredTrans = null;
            
            
            yield return new WaitForSeconds(.3f);
            CameraMovementController.Instance.defaultForwardVelocity += .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
        }
    }
    IEnumerator PlayerRightKickRoutine(Transform t_Transform)
    {
       // if (m_CurrentTriggeredTrans != null)
        {
            playerAnimator.SetTrigger(RIGHT_KICK);
            yield return new WaitForSeconds(.25f);
            rightHitParticle.Play();
            yield return new WaitForEndOfFrame();
            t_Transform.GetComponent<NPCCharacter>().MakeCharacterIdle();
            t_Transform.GetChild(0).GetComponent<Animator>().enabled = false;

            yield return new WaitForEndOfFrame();
           
          
            m_IsSpeedBoost = true;
            speedParticle.Play();
            
            CameraMovementController.Instance.defaultForwardVelocity -= .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
            yield return new WaitForSeconds(.2f);
            m_CurrentTriggeredTrans = null;
            
            
            yield return new WaitForSeconds(.3f);
            CameraMovementController.Instance.defaultForwardVelocity += .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
        }
    }

    public void IncreaseMovmentSpeed(float t_IncreaseFactor)
    {
        movementSpeed += t_IncreaseFactor;
        
        if (IsMaxSpeed())
        {
            movementSpeed = maxSpeed;
        }
        
    }
    
    private void RotatePlayer(float t_RotationValue)
    {
        playerTrans.rotation = Quaternion.Euler(0,t_RotationValue,0);
    }
    
    private void MoveObject(Vector3 t_Direction)
    {
        Vector3 t = Vector3.forward + t_Direction;
        playerTrans.Translate(t * movementSpeed *Time.deltaTime);

        if (playerTrans.position.x > 7f)
        {
            playerTrans.position = new Vector3(7f,playerTrans.position.y,playerTrans.position.z);
        }

        if (playerTrans.position.x < -8f)
        {
            playerTrans.position = new Vector3(-8f,playerTrans.position.y,playerTrans.position.z);
        }
        
    }

    public void Triggerred(KickSide kickSide1,Transform t_TriggeredTrans)
    {
        m_CurrentTriggeredTrans = t_TriggeredTrans;
        //Debug.LogError(m_CurrentTriggeredTrans.name);
        kickSide = kickSide1;
        
        HitOrKick(kickSide,t_TriggeredTrans);
    }

    public void IncreaseFactors()
    {
        movementSpeed *=2.2f;
        CameraMovementController.Instance.defaultForwardVelocity += 0.35f;
        
        CameraMovementController.Instance.defaultCameraPositionOffset = new Vector3(
            CameraMovementController.Instance.defaultCameraPositionOffset.x,
            CameraMovementController.Instance.defaultCameraPositionOffset.y,
            CameraMovementController.Instance.defaultCameraPositionOffset.z-3.5f);

        m_ChaseCar = true;
    }

   

    public bool IsMaxSpeed()
    {
        if (movementSpeed >= maxSpeed)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public void TriggeredNPCCar(KickSide kickSide1, Transform otherTransform)
    {
        if (kickSide1 == KickSide.LEFT)
        {
            StartCoroutine(LeftCarKickRoutine(otherTransform));
        }
        else if (kickSide1 == KickSide.RIGHT)
        {
            StartCoroutine(RightCarKickRoutine(otherTransform));
        }
    }

    public void TriggeredNPCBike(KickSide t_KickSide, Transform t_OtherTransform)
    {
        if (t_KickSide == KickSide.LEFT)
        {
            int t_Rand = Random.Range(0, 100);
            if (t_Rand < 60)
            {
                StartCoroutine(LeftBikeKickRoutine(t_OtherTransform));
            }
            else
            {
                StartCoroutine(LeftBikeHitRoutine(t_OtherTransform));
            }
        }
        else if (t_KickSide == KickSide.RIGHT)
        {
            int t_Rand = Random.Range(0, 100);
            if (t_Rand < 60)
            {
                StartCoroutine(RightBikeKickRoutine(t_OtherTransform));
            }
            else
            {
                StartCoroutine(RightBikeHitRoutine(t_OtherTransform));
            }
        }
    }
    
    IEnumerator RightBikeHitRoutine(Transform t_OtherTransform)
    {
        playerAnimator.SetTrigger(RIGHT_HIT);
        yield return new WaitForSeconds(0.25f);
        
        rightBaton.transform.SetParent(rightBatonTrans);
        rightBaton.transform.localPosition = Vector3.zero;
        rightBaton.transform.localRotation = Quaternion.Euler(new Vector3(0,-225,0));
        
       // if (m_CurrentTriggeredTrans != null)
        {
            leftHitParticle.Play();

            yield return new WaitForEndOfFrame();
            t_OtherTransform.GetComponent<NPCBike>().CreateExploison();
            
            
            m_IsSpeedBoost = true;
            speedParticle.Play();

            CameraMovementController.Instance.defaultForwardVelocity -= .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
            
            yield return new WaitForSeconds(.2f);
            m_CurrentTriggeredTrans = null;
            
            yield return new WaitForSeconds(.3f);
            CameraMovementController.Instance.defaultForwardVelocity += .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
        }
        rightBaton.transform.SetParent(rightBatonParentTrans);
        rightBaton.transform.localPosition = m_RightBattonInitPos;
        rightBaton.transform.localRotation = ( m_RightBattonInitRot);
    }
    
    IEnumerator LeftBikeHitRoutine(Transform t_OtherTransform)
    {
        playerAnimator.SetTrigger(RIGHT_HIT);
        yield return new WaitForSeconds(0.25f);
        
        rightBaton.transform.SetParent(rightBatonTrans);
        rightBaton.transform.localPosition = Vector3.zero;
        rightBaton.transform.localRotation = Quaternion.Euler(new Vector3(0,-225,0));
        
        //if (m_CurrentTriggeredTrans != null)
        {
            leftHitParticle.Play();

            yield return new WaitForEndOfFrame();
            t_OtherTransform.GetComponent<NPCBike>().LeftExploision();
            
            
            m_IsSpeedBoost = true;
            speedParticle.Play();

            CameraMovementController.Instance.defaultForwardVelocity -= .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
            
            yield return new WaitForSeconds(.2f);
            m_CurrentTriggeredTrans = null;
            
            yield return new WaitForSeconds(.3f);
            CameraMovementController.Instance.defaultForwardVelocity += .35f;
            CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
        }
        rightBaton.transform.SetParent(rightBatonParentTrans);
        rightBaton.transform.localPosition = m_RightBattonInitPos;
        rightBaton.transform.localRotation = ( m_RightBattonInitRot);
    }
    
    
    

    IEnumerator LeftCarKickRoutine(Transform t_CarTrans)
    {
        playerAnimator.SetTrigger(LEFT_KICK);
        yield return new WaitForSeconds(.2f);
        rightHitParticle.Play();
        
        t_CarTrans.GetComponent<NPCCar>().CreateExploison();
        
        m_IsSpeedBoost = true;
        speedParticle.Play();


        yield return new WaitForEndOfFrame();

        CameraMovementController.Instance.defaultForwardVelocity -= .35f;
         CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
            
       
         yield return new WaitForSeconds(.4f);
         CameraMovementController.Instance.defaultForwardVelocity += .35f;
         CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);

         
         
        IncreaseMovmentSpeed(1);
    }
    IEnumerator RightCarKickRoutine(Transform t_CarTrans)
    {
        playerAnimator.SetTrigger(RIGHT_KICK);
        yield return new WaitForSeconds(.2f);
        rightHitParticle.Play();
        
        t_CarTrans.GetComponent<NPCCar>().RightCreateExploision();
        
        m_IsSpeedBoost = true;
        speedParticle.Play();


        yield return new WaitForEndOfFrame();

        CameraMovementController.Instance.defaultForwardVelocity -= .35f;
        CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
            
       
        yield return new WaitForSeconds(.4f);
        CameraMovementController.Instance.defaultForwardVelocity += .35f;
        CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);

         
         
        IncreaseMovmentSpeed(1);
    }

    IEnumerator RightBikeKickRoutine(Transform t_BikeTrans)
    {
        playerAnimator.SetTrigger(RIGHT_KICK);
        yield return new WaitForSeconds(.2f);
        rightHitParticle.Play();
        
        t_BikeTrans.GetComponent<NPCBike>().CreateExploison();
        
        m_IsSpeedBoost = true;
        speedParticle.Play();
        
        yield return new WaitForEndOfFrame();
        
        CameraMovementController.Instance.defaultForwardVelocity -= .35f;
        CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
            
       
        yield return new WaitForSeconds(.4f);
        CameraMovementController.Instance.defaultForwardVelocity += .35f;
        CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);

        IncreaseMovmentSpeed(1);
    }
    IEnumerator LeftBikeKickRoutine(Transform t_BikeTrans)
    {
        playerAnimator.SetTrigger(LEFT_KICK);
        yield return new WaitForSeconds(.2f);
        rightHitParticle.Play();
        
        t_BikeTrans.GetComponent<NPCBike>().LeftExploision();
        
        m_IsSpeedBoost = true;
        speedParticle.Play();
        
        yield return new WaitForEndOfFrame();
        
        CameraMovementController.Instance.defaultForwardVelocity -= .35f;
        CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);
            
       
        yield return new WaitForSeconds(.4f);
        CameraMovementController.Instance.defaultForwardVelocity += .35f;
        CameraMovementController.Instance.FocusCameraWithOrigin(originTrans,targetOriginTrans);

        IncreaseMovmentSpeed(1);
    }

    public void TriggeredNPCBoss(KickSide t_KickSide, Transform t_OtherTransform)
    {
        if (t_KickSide == KickSide.LEFT)
        {
            StartCoroutine(LeftBossKickRoutine(t_OtherTransform));
        }
        else if (t_KickSide == KickSide.RIGHT)
        {
            StartCoroutine(RightBossKickRoutine(t_OtherTransform));
        }
    }

    private IEnumerator RightBossKickRoutine(Transform t_BossTrans)
    {
        playerAnimator.transform.position = playerAnimator.transform.position + Vector3.up * 1.35f;
        playerAnimator.SetTrigger(RIGHT_KICK);
        yield return new WaitForSeconds(.2f);
        rightHitParticle.Play();
        
        //t_BossTrans.GetComponent<NPCBike>().CreateExploison();
        if (t_BossTrans.GetComponent<NPCBoss>().carMovementSpeed > movementSpeed)
        {
            float t_Diff = t_BossTrans.GetComponent<NPCBoss>().carMovementSpeed - movementSpeed;

            movementSpeed += t_Diff;
        }
        else
        {
            float t_Diff = movementSpeed - t_BossTrans.GetComponent<NPCBoss>().carMovementSpeed;

            movementSpeed -= t_Diff;
        }
        
        yield return  new WaitForSeconds(0.75f);
        playerAnimator.transform.position =playerAnimator.transform.position - Vector3.up * 1.35f;
        
        
        
        IncreaseMovmentSpeed(1);
    }

    private IEnumerator LeftBossKickRoutine(Transform t_BossTrans)
    {
        playerAnimator.transform.position = playerAnimator.transform.position + Vector3.up * 1.35f;
        playerAnimator.SetTrigger(LEFT_KICK);
        yield return new WaitForSeconds(.2f);
        rightHitParticle.Play();
        
        if (t_BossTrans.GetComponent<NPCBoss>().carMovementSpeed > movementSpeed)
        {
            float t_Diff = t_BossTrans.GetComponent<NPCBoss>().carMovementSpeed - movementSpeed;

            movementSpeed += t_Diff;
        }
        else
        {
            float t_Diff = movementSpeed - t_BossTrans.GetComponent<NPCBoss>().carMovementSpeed;

            movementSpeed -= t_Diff;
        }
        
        yield return  new WaitForSeconds(0.75f);
        playerAnimator.transform.position =playerAnimator.transform.position - Vector3.up * 1.35f;
           
        IncreaseMovmentSpeed(1);
    }

    public void PlayBikeIdle()
    {
        playerAnimator.SetTrigger(BIKE_IDLE);
    }

    public void PlayerJump(Transform oldTrans)
    {
        oldTrans.GetComponent<NPCOld>().OldTrigger();
        
        StartCoroutine(PlayerJumpRoutine());
    }

    private IEnumerator PlayerJumpRoutine()
    {
        playerTrans.position += Vector3.up * 1.25f;
        
        yield return new WaitForSeconds(1f);
        
        playerTrans.position -= Vector3.up*1.25f;
    }
}

public enum KickSide
{
    NONE,
    LEFT,
    RIGHT
}
