﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ObjectList))]
public class NPCRoamingManager : MonoBehaviour
{
    #region Custom Variables

    [System.Serializable]
    public struct NPCInfo
    {
        public int checkPointIndex;
        public float npcMovementSpeed;
        public Transform npcTransformReference;
    }

    [System.Serializable]
    public struct NPCCheckPoint
    {
        public Transform checkPointPosition;
        [Range(0,360)]
        public float rotationValue;
    }

    #endregion

    #region Public Variables

    [Range(0,1f)]
    public float spawnFrequency;

    [Space(5.0f)]
    [Range(0.0f,0.5f)]
    public float npcSpeedVariation;
    [Range(00f,10.0f)]
    public float npcMovementSpeed;

    [Space(5.0f)]
    public NPCCheckPoint[] npcCheckpoints;

    #endregion

    #region Private Variables

    private ObjectList m_NPCListReference;

    private List<NPCInfo> m_ActiveNPC;

    private bool m_IsNPCControllerForSpawnerRunning;
    private bool m_IsNPCControllerForMovementRunning;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_NPCListReference = gameObject.GetComponent<ObjectList>();
    }

    private void Start()
    {
        //Debug Purpose
        PreProcess();
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForNPCSpawner() {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        float t_TimeDifferenceOnSpawn = (1f - spawnFrequency) * 60f;
        float t_RemainingTimeToSpawn = Random.Range(0,5);

        while (m_IsNPCControllerForSpawnerRunning) {

            if (t_RemainingTimeToSpawn <= 0)
            {

                t_RemainingTimeToSpawn = Random.Range(t_TimeDifferenceOnSpawn / 2.0f, t_TimeDifferenceOnSpawn);
                
                GameObject t_NewNPC = Instantiate(
                        m_NPCListReference.GetObject(null),
                        transform.position,
                        Quaternion.identity);

                Transform t_TransformReference = t_NewNPC.transform;
                t_TransformReference.SetParent(transform);
                t_TransformReference.position = npcCheckpoints[0].checkPointPosition.position;
                t_TransformReference.eulerAngles = new Vector3(
                        0,
                        npcCheckpoints[0].rotationValue,
                        0
                    );

                NPCInfo t_NewNPCInfo = new NPCInfo()
                {
                    checkPointIndex = 0,
                    npcTransformReference = t_TransformReference,
                    npcMovementSpeed = Random.Range((npcMovementSpeed - (npcMovementSpeed * npcSpeedVariation)), npcMovementSpeed)
                };

                m_ActiveNPC.Add(t_NewNPCInfo);
            }
            else {

                t_RemainingTimeToSpawn -= t_CycleLength;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForNPCSpawner());
    }

    private IEnumerator ControllerForNPCMovement() {

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        int t_ResetCheckPointIndex = npcCheckpoints.Length - 1;
        int t_NumberOfActiveNPC = 1;
        float t_DeltaTime;
        Vector3 t_ModifiedPosition;
        NPCInfo t_ModifiedNPCInfo;

        int t_NumberOfNPCToDestroy;
        List<int> t_IndexOfNPCToDestroy = new List<int>();

        while (m_IsNPCControllerForSpawnerRunning || t_NumberOfActiveNPC > 0) {
            

            t_DeltaTime = Time.deltaTime;

            t_NumberOfActiveNPC = m_ActiveNPC.Count;
            for (int npcIndex = 0; npcIndex < t_NumberOfActiveNPC; npcIndex++) {

                t_ModifiedNPCInfo = m_ActiveNPC[npcIndex];

                if (Vector3.Distance(
                    t_ModifiedNPCInfo.npcTransformReference.position,
                    npcCheckpoints[t_ModifiedNPCInfo.checkPointIndex].checkPointPosition.position) <= 0.1f)
                {

                    if (t_ModifiedNPCInfo.checkPointIndex == t_ResetCheckPointIndex)
                    {
                        // if : Last Check Point
                        t_IndexOfNPCToDestroy.Add(npcIndex);
                    }
                    else
                    {

                        t_ModifiedNPCInfo.npcTransformReference.eulerAngles = new Vector3(
                            0f,
                            npcCheckpoints[t_ModifiedNPCInfo.checkPointIndex].rotationValue,
                            0f
                        );

                        t_ModifiedNPCInfo.checkPointIndex += 1;
                        m_ActiveNPC[npcIndex] = t_ModifiedNPCInfo;
                    }
                }
                else {
                    
                    t_ModifiedPosition = Vector3.MoveTowards(
                            t_ModifiedNPCInfo.npcTransformReference.position,
                            npcCheckpoints[t_ModifiedNPCInfo.checkPointIndex].checkPointPosition.position,
                            t_ModifiedNPCInfo.npcMovementSpeed * t_DeltaTime
                        );
                    t_ModifiedNPCInfo.npcTransformReference.position = t_ModifiedPosition;
                }
            }

            //Clear List
            t_NumberOfNPCToDestroy = t_IndexOfNPCToDestroy.Count;
            for (int npcIndex = 0; npcIndex < t_NumberOfNPCToDestroy; npcIndex++) {

                Destroy(m_ActiveNPC[t_IndexOfNPCToDestroy[npcIndex]].npcTransformReference.gameObject);
                m_ActiveNPC.RemoveAt(t_IndexOfNPCToDestroy[npcIndex]);
            }

            t_IndexOfNPCToDestroy.Clear();
            m_ActiveNPC.TrimExcess();

            yield return t_CycleDelay;
        }

        m_IsNPCControllerForMovementRunning = false;
        m_ActiveNPC = null;

        StopCoroutine(ControllerForNPCMovement());
    }

    #endregion

    #region Public Callback

    public void PreProcess() {

        if (m_ActiveNPC == null) {

            m_ActiveNPC = new List<NPCInfo>();
        }

        if (!m_IsNPCControllerForSpawnerRunning) {

            m_IsNPCControllerForSpawnerRunning = true;
            StartCoroutine(ControllerForNPCSpawner());
        }

        if (!m_IsNPCControllerForMovementRunning) {

            m_IsNPCControllerForMovementRunning = true;
            StartCoroutine(ControllerForNPCMovement());
        }
    }

    public void PostProcess() {

        m_IsNPCControllerForSpawnerRunning = false;
    }

    public void StopSpawn()
    {
        Debug.LogError("SPAWN OFF");
        StopCoroutine(ControllerForNPCSpawner());
    }

    #endregion
}
