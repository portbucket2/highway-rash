﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCOld : MonoBehaviour
{
    public Animator animator;
    public Transform initTrans;
    
    private int WALK = Animator.StringToHash("walk");
    public bool isStartMove;

    private Vector3 initPos;
    

    private void Start()
    {
        initPos = transform.position;
    }

    private void Update()
    {
        if (isStartMove)
        {
            transform.position += Vector3.left * 1f * Time.deltaTime;
        }
    }

    public void Triggered()
    {
        animator.SetTrigger(WALK);
        isStartMove = true;

        StartCoroutine(OldReset());
    }

   



    IEnumerator OldReset()
    {
        yield return new WaitForSeconds(7);
        gameObject.SetActive(false);
        yield return new WaitForSeconds(1);
        gameObject.SetActive(true);
        transform.position = initTrans.position;
        animator.enabled = true;
        isStartMove = false;
        
    }

    public void OldTrigger()
    {
        StartCoroutine(OldTriggerdRoutine());
    }

    IEnumerator OldTriggerdRoutine()
    {
        GetComponent<BoxCollider>().enabled = false;
        animator.enabled = false;
        
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(true);
        GetComponent<BoxCollider>().enabled = true;
        transform.position = initTrans.position;
        animator.enabled = true;
        isStartMove = false;
    }

    public void ResetAll()
    {
        gameObject.SetActive(false);
        gameObject.SetActive(true);
        GetComponent<BoxCollider>().enabled = true;
        transform.position = initTrans.position;
        animator.enabled = true;
        isStartMove = false;
    }
}
