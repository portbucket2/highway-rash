﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SkillTree))]
public class PowerUpController : MonoBehaviour {


	[System.Serializable]
	public struct PowerUp
	{
		public string skillName;
		[Tooltip("if 'isStakable = true', the power up going to be stack if you take again. Else, it will be reset")]
		public bool isStackable;
		[Range(0.0f,20.0f)]
		public float baseDuration;
		public UnityEvent OnPowerUpStart;
		public UnityEvent OnPowerUpEnd;

		[Space(5.0f)]
		[Header("Pre Warning State")]
		[Range(0.0f,1.0f)]
		public float warningBeforeEndTime;
		public UnityEvent OnPreWarningForEndOfPowerUp;

		[HideInInspector]
		public bool 	m_IsActive;
		[HideInInspector]
		public float 	m_EndTimeForPowerUp;
		
		[HideInInspector]
		public bool 	m_IsPreWarningEventTriggered;
		[HideInInspector]
		public float	m_PreEndTimeForPowerUp;
	}


	//----------
	#region Public Variables

	public PowerUp[] powerUp;

	#endregion

	//----------
	#region Private Varibles

	private SkillTree 	powerUpSkillTree;
	private bool 		m_RunPowerUpLifeCycleController;

	#endregion

	//----------
	#region Mono Behaviour

	void Awake(){

		PreProcess();
	}

	#endregion

	//----------
	#region PowerUp Configuretion

	private void PreProcess(){

		powerUpSkillTree = gameObject.GetComponent<SkillTree>();
	}

	private bool IsAnyPowerUpActive(){
		
		bool m_Decision = false;
		
		for(int powerUpIndex = 0; powerUpIndex < powerUp.Length ; powerUpIndex++){

			if(powerUp[powerUpIndex].m_IsActive){
				m_Decision = true;
				break;
			}
		}

		return m_Decision;
	}

	private void PowerUpLifeCycleControlManager(){

		if(IsAnyPowerUpActive()){

			if(!m_RunPowerUpLifeCycleController){

				m_RunPowerUpLifeCycleController = true;
				StartCoroutine(PowerUpLifeCycleController());
			}
		}else
			m_RunPowerUpLifeCycleController = false;
	}

	private IEnumerator PowerUpLifeCycleController(){

		while(m_RunPowerUpLifeCycleController){

			yield return new WaitForEndOfFrame();
			for(int powerUpIndex = 0; powerUpIndex < powerUp.Length ; powerUpIndex++){

				if(!powerUp[powerUpIndex].m_IsPreWarningEventTriggered && powerUp[powerUpIndex].m_IsActive && (Time.time > powerUp[powerUpIndex].m_PreEndTimeForPowerUp)){

					powerUp[powerUpIndex].m_IsPreWarningEventTriggered = true;
					powerUp[powerUpIndex].OnPreWarningForEndOfPowerUp.Invoke();
				}else if(powerUp[powerUpIndex].m_IsActive && (Time.time > powerUp[powerUpIndex].m_EndTimeForPowerUp)){

					powerUp[powerUpIndex].m_IsActive 			= false;
					powerUp[powerUpIndex].m_EndTimeForPowerUp 	= 0.0f;
					powerUp[powerUpIndex].OnPowerUpEnd.Invoke();
					PowerUpLifeCycleControlManager();
					break;
				}
			}
		}

		StopCoroutine(PowerUpLifeCycleController());
	}

	#endregion

	//----------
	#region Public Callback : Activating PowerUp

	public bool IsPowerUpActive(string skillName){

		bool 	m_IsValidIndex 	= false;
		int 	m_SkillIndex 	= 0;
		
		for(int powerUpIndex = 0; powerUpIndex < powerUp.Length ; powerUpIndex++){

			if(powerUp[powerUpIndex].skillName == skillName){
				m_SkillIndex 	= powerUpIndex;
				m_IsValidIndex 	= true;
				break;
			}
		}

		if(m_IsValidIndex)
			return IsPowerUpActive(m_SkillIndex);
		else{
			Debug.LogAssertion("Invalid SkillName");
			return false;
		}
	}

	public bool IsPowerUpActive(int skillIndex){

		if(skillIndex < powerUp.Length){

			if(powerUp[skillIndex].m_IsActive)
				return true;
			else 
				return false;
		}else{
			Debug.LogAssertion("Invalid Index for power up");
			return false;
		}
	}

	public void ActivatePowerUp(string skillName){

		bool 	m_IsValidIndex 	= false;
		int 	m_SkillIndex 	= 0;
		
		for(int powerUpIndex = 0; powerUpIndex < powerUp.Length ; powerUpIndex++){

			if(powerUp[powerUpIndex].skillName == skillName){
				m_SkillIndex 	= powerUpIndex;
				m_IsValidIndex 	= true;
				break;
			}
		}

		if(m_IsValidIndex)
			ActivatePowerUp(m_SkillIndex);
		else
			Debug.LogAssertion("Invalid SkillName");
	}

	public void ActivatePowerUp(int skillIndex){

		if(skillIndex < powerUp.Length){

			if(powerUp[skillIndex].isStackable && powerUp[skillIndex].m_EndTimeForPowerUp != 0.0f){

				powerUp[skillIndex].m_PreEndTimeForPowerUp 	+= (powerUp[skillIndex].baseDuration + powerUpSkillTree.GetCurrentStatOfSkill(powerUp[skillIndex].skillName)) * powerUp[skillIndex].warningBeforeEndTime;
				powerUp[skillIndex].m_EndTimeForPowerUp 	+= (powerUp[skillIndex].baseDuration + powerUpSkillTree.GetCurrentStatOfSkill(powerUp[skillIndex].skillName));
			}else{

				if(powerUp[skillIndex].m_EndTimeForPowerUp == 0.0f){
					powerUp[skillIndex].m_PreEndTimeForPowerUp 	= Time.time + (powerUp[skillIndex].baseDuration + powerUpSkillTree.GetCurrentStatOfSkill(powerUp[skillIndex].skillName)) * powerUp[skillIndex].warningBeforeEndTime; 
					powerUp[skillIndex].m_EndTimeForPowerUp 	= Time.time + (powerUp[skillIndex].baseDuration + powerUpSkillTree.GetCurrentStatOfSkill(powerUp[skillIndex].skillName));

					powerUp[skillIndex].m_IsActive = true;
					powerUp[skillIndex].m_IsPreWarningEventTriggered 	= false;
					powerUp[skillIndex].OnPowerUpStart.Invoke();
					PowerUpLifeCycleControlManager();
				}
			}
		}else
			Debug.LogAssertion("Invalid Index for power up");	

	}

	#endregion
}
