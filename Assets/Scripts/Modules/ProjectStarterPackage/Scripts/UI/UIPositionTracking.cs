﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPositionTracking : MonoBehaviour {

	public RectTransform UIPosition;

	void Start () {

		transform.position = new Vector3(
			UIPosition.position.x,
			UIPosition.position.y,
			transform.position.z
		);
	}
	
}
