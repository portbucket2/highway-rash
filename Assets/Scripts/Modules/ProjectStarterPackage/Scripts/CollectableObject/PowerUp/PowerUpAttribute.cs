﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpAttribute : MonoBehaviour {

	public enum PowerUp
	{
		magnet,
		shield,
		doubleDamage
	};

	public PowerUp typeOfPowerUp;

	void Start(){

		PowerUpManager.Instance.AddPowerUp(transform);
	}

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this
	/// object (2D physics only).
	/// </summary>
	/// <param name="other">The other Collider2D involved in this collision.</param>
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player"){

			switch(typeOfPowerUp){
				case PowerUp.magnet:
				CoinManager.Instance.EnableMagnetAsPowerUp(5.0f);
				break;
				case PowerUp.shield:
				
				break;
				case PowerUp.doubleDamage:
				
				break;
			}
			
			PowerUpManager.Instance.RemovePowerUp(transform);
			ExecuteDestroyAnimation();
		}
	}

	public void ExecuteDestroyAnimation(){
		
		Destroy(gameObject);
	}
}
