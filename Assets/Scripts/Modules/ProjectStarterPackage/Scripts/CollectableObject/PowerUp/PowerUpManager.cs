﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour {

	public static PowerUpManager Instance;

	#region Public Variables

	public Transform spawnedPowerUpParent;
	[Range (1.0f, 10.0f)]
	public float powerUpFallingSpeed;
	[Space (2.5f)]
	public UnitVector2D powerUpFallingDirection;
	[Range (0.0f, 25.0f)]
	public float distanceToBeTravel;

	#endregion

	//----------------------------------------------------------------------
	#region Private Variables

	private List<Transform> m_ActivePowerUpOnMap;

    private float m_CurrentGameSpeed;

    #endregion

    //----------------------------------------------------------------------
    #region Mono Behavior

    //----------------------------------------------------------------------
    void Awake(){

		Instance = this;
	}

    void Start()
    {
        m_CurrentGameSpeed = GameManager.Instance.GetGameSpeed();
        GameManager.Instance.RegisterEventOnChangingGameSpeec(
            delegate
            {
                m_CurrentGameSpeed = GameManager.Instance.GetGameSpeed();
            });
    }

    #endregion

    //----------------------------------------------------------------------
    #region Public Callback

    public void PreProcess () {

		m_ActivePowerUpOnMap = new List<Transform>();
		StartPowerUpController();
	}

	public void RestoreToDefault(){

		StopPowerUpController();
	}

	public void StartPowerUpController(){
		m_IsPowerUpControllerRunning = true;
		StartCoroutine(PowerUpController());
	}

	public void StopPowerUpController(){
		m_IsPowerUpControllerRunning = false;
	}

	public void AddPowerUp(Transform t_PowerUpTransformReference){

		t_PowerUpTransformReference.parent = spawnedPowerUpParent;
		m_ActivePowerUpOnMap.Add(t_PowerUpTransformReference);
	}

	public void RemovePowerUp(Transform t_PowerUpTransformReference){

		m_ActivePowerUpOnMap.Remove(t_PowerUpTransformReference);
	}

	#endregion

	//----------------------------------------------------------------------
	#region Configuretion

	private bool m_IsPowerUpControllerRunning;

	private IEnumerator PowerUpController(){

		WaitUntil t_GamePauseResumeState = new WaitUntil (() => {
			if (GameManager.Instance.IsGamePaused ())
				return false;
			else
				return true;
		});
		WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

		Vector2 t_Direction = new Vector2(
			powerUpFallingDirection.x,
			powerUpFallingDirection.y
		);
		float t_DestinationPoint = t_Direction.y * distanceToBeTravel;

		int t_NumberOfActivePowerUp;
		Vector2 t_Displacement;

		while(m_IsPowerUpControllerRunning){

			yield return t_GamePauseResumeState;
			
			t_NumberOfActivePowerUp = m_ActivePowerUpOnMap.Count;

			t_Displacement = t_Direction * powerUpFallingSpeed * Time.deltaTime;

			for(int powerUpIndex = 0; powerUpIndex < t_NumberOfActivePowerUp ; powerUpIndex++){
				
				m_ActivePowerUpOnMap[powerUpIndex].Translate(
					t_Displacement
				);

				if(Mathf.Abs(m_ActivePowerUpOnMap[powerUpIndex].position.y - t_DestinationPoint) <= .1f){

					Destroy(m_ActivePowerUpOnMap[powerUpIndex].gameObject);
					m_ActivePowerUpOnMap.RemoveAt(powerUpIndex);
					break;
				}
			}

			yield return t_CycleDelay;
		}

		t_NumberOfActivePowerUp = m_ActivePowerUpOnMap.Count;
		for(int index = 0; index < t_NumberOfActivePowerUp; index++){
			m_ActivePowerUpOnMap[index].GetComponent<PowerUpAttribute>().ExecuteDestroyAnimation();
		}

		m_ActivePowerUpOnMap.Clear();
		StopCoroutine(PowerUpController());
	}

	#endregion
}
