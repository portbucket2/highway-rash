﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (GameplayManager))]
public class GameplayManagerEditor : Editor {

	public GameplayManager Reference;

	#region Configuretion		:	Editor

	void OnEnable () {

		Reference = (GameplayManager) target;

		if (GameplayManager.Instance == null) {

			GameplayManager.Instance = Reference;
		}
	}

	public override void OnInspectorGUI () {

		serializedObject.Update ();

		GameDifficultyLevelInfoUI ();

		DrawDefaultInspector ();

		serializedObject.ApplyModifiedProperties ();
	}

	#endregion

	//----------
	#region Configuretion		:	PreData Display

	private void CheckForDataConfiguretion () {

		for (int difficultyLevelIndex = 0; difficultyLevelIndex < Reference.difficultyLevel.Length; difficultyLevelIndex++) {

			Reference.difficultyLevel[difficultyLevelIndex].levelTitle = "Level (" + difficultyLevelIndex + ")";

			long t_ScoreBound = 0;

			if (Reference.increaseByPercentage) {

                t_ScoreBound = Reference.baseValueOfDifficultyLevel;

                if (difficultyLevelIndex == 0) {

                    if (Reference.IsLevelResetOnEveryGame)
                        t_ScoreBound = 0;
                    else
                        t_ScoreBound = Reference.baseValueOfDifficultyLevel;
				} else {

                    if (Reference.IsLevelResetOnEveryGame && difficultyLevelIndex == 1)
                    {
                        t_ScoreBound = Reference.baseValueOfDifficultyLevel;
                    }
                    else {

                        long t_RequiredObjectiveToReachThatLevel = Reference.difficultyLevel[difficultyLevelIndex - 1].requiredObjectiveToReachedThisLevel;
                        t_ScoreBound = (t_RequiredObjectiveToReachThatLevel * (difficultyLevelIndex >=2 ? 2 : difficultyLevelIndex)) + (long)(t_RequiredObjectiveToReachThatLevel * Reference.percentageToIncreaseByLevel);
                    }
				}

			} else {

                if (difficultyLevelIndex > 0)
                {
                    t_ScoreBound = Reference.difficultyLevel[difficultyLevelIndex - 1].requiredObjectiveToReachedThisLevel;
                }
                else {

                    t_ScoreBound = Reference.baseValueOfDifficultyLevel;
                }

				t_ScoreBound += (long) Reference.increasingValue;
			}

            t_ScoreBound = t_ScoreBound > Reference.maxLimitOfObjective ? Reference.maxLimitOfObjective : t_ScoreBound;

			Reference.difficultyLevel[difficultyLevelIndex].requiredObjectiveToReachedThisLevel = t_ScoreBound;
		}
	}

	#endregion

	//----------
	#region Configuretion		:	UI Call

	private void GameDifficultyLevelInfoUI () {

		if (Reference.difficultyLevel != null) {

			CheckForDataConfiguretion ();

			GUILayout.Space (5.0f);
			EditorGUILayout.BeginVertical (); {

				EditorGUILayout.LabelField ("Score 	 : " + Reference.GetCurrentScore ());

				EditorGUILayout.LabelField ("Level (Play): " + Reference.GetCurrentDifficultyLevel ());

				if (GUILayout.Button ("Increase Level")) {

                    for (int i = 0; i < 5; i++)
                        Reference.IncreaseLevel();
				}

				if (GUILayout.Button ("Reset Level")) {

					Reference.ResetLevel();

					Reference.ResetScore ();
				}

			}
			EditorGUILayout.EndVertical ();

			GUILayout.Space (5.0f);
			EditorGUILayout.BeginVertical (); {

				if (GUILayout.Button ("Generate EnemySetID")) {

					int t_MaxRequiredLevel = 45;
					int t_NumberOfLevel = Reference.difficultyLevel.Length;
					int t_NumberOfEnemySet = Reference.enemySet.Length;
					float[] t_LevelBasedDifficulty = new float[t_NumberOfLevel];
					for (int difficultyLevelIndex = 0; difficultyLevelIndex < t_NumberOfLevel; difficultyLevelIndex++) {

						List<EnemySet> t_EnemySet = new List<EnemySet> ();
						for (int enemySetIndex = 0; enemySetIndex < t_NumberOfEnemySet; enemySetIndex++) {

							if (Reference.enemySet[enemySetIndex].requiredLevel <= difficultyLevelIndex) {
								t_EnemySet.Add (Reference.enemySet[enemySetIndex]);
							}
						}

						//Previous Track Recorder
						int t_TrackerBoundary = difficultyLevelIndex > 4 ? (difficultyLevelIndex - 4) : 0;
						int t_Divider = 0;
						float t_AvgDifficulty = 0f;
						for (int trackerIndex = difficultyLevelIndex; trackerIndex >= t_TrackerBoundary; trackerIndex--) {

							t_AvgDifficulty += t_LevelBasedDifficulty[trackerIndex];
							t_Divider++;

						}
						t_AvgDifficulty /= (t_Divider == 0 ? 1 : t_Divider);

						int t_NumberOfPeekedEnemySet = t_EnemySet.Count;
						float[] t_Probability = new float[t_NumberOfPeekedEnemySet];
						for (int enemySetIndex = 0; enemySetIndex < t_NumberOfPeekedEnemySet; enemySetIndex++) {

							t_Probability[enemySetIndex] = (((t_EnemySet[enemySetIndex].requiredLevel + 1) / (float) t_MaxRequiredLevel) * 0.5f) + (((enemySetIndex + 1.0f) / t_NumberOfEnemySet) * t_AvgDifficulty * 0.5f);
							//Debug.Log("DI (" + difficultyLevelIndex +") : " + ((t_EnemySet[enemySetIndex].requiredLevel + 1) / (float) t_MaxRequiredLevel) + " : " +  t_AvgDifficulty);
						}

						PriorityBound[] t_PriorityBound = MathFunction.Instance.GetPriorityBound (t_Probability);
						float t_SelectionProbability = Random.Range (0f, 1f);
						for (int probabilityIndex = 0; probabilityIndex < t_PriorityBound.Length; probabilityIndex++) {
							if (t_SelectionProbability >= t_PriorityBound[probabilityIndex].lowerPriority && t_SelectionProbability < t_PriorityBound[probabilityIndex].higherPriority) {

								Reference.difficultyLevel[difficultyLevelIndex].enemySet = t_EnemySet[probabilityIndex];
								t_LevelBasedDifficulty[difficultyLevelIndex] = (((t_EnemySet[probabilityIndex].requiredLevel + 1) / (float) t_MaxRequiredLevel) * 0.5f) + (((probabilityIndex + 1.0f) / t_NumberOfEnemySet) * t_AvgDifficulty * 0.5f);
								//Debug.Log("DF (" + difficultyLevelIndex + ") : " + t_LevelBasedDifficulty[difficultyLevelIndex]);
								break;
							}
						}
					}
				}

				if (GUILayout.Button ("Reset HighScore")) {
					Reference.ResetHighScore ();
				}

			}
			EditorGUILayout.EndVertical ();

			GUILayout.Space (5.0f);
			EditorGUILayout.BeginVertical (); {

				if (Reference.isShowLevelInfo) {
					for (int difficultyLevelIndex = 0; difficultyLevelIndex < Reference.difficultyLevel.Length; difficultyLevelIndex++) {

						EditorGUILayout.LabelField (
							Reference.difficultyLevel[difficultyLevelIndex].levelTitle +
							" : Required Objective (" + Reference.difficultyLevel[difficultyLevelIndex].requiredObjectiveToReachedThisLevel +
							")" +
							(Reference.difficultyLevel[difficultyLevelIndex].enemySet.enemyListReference != null ? (", ColorSet(" + Reference.difficultyLevel[difficultyLevelIndex].enemySet.themeColorIndex + "), EnemySet(" + Reference.difficultyLevel[difficultyLevelIndex].enemySet.enemyListReference.name + ")") : ""));
					}
				}
			}
			EditorGUILayout.EndVertical ();

		}

		GUILayout.Space (5.0f);
	}

	#endregion
}