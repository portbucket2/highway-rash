﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct GameDifficultyLevel {

#if UNITY_EDITOR
	[HideInInspector]
	public string levelTitle;
#endif

	public EnemySet enemySet;
	public UnityEvent OnDifficultyLevelReached;

	[HideInInspector]
	public long requiredObjectiveToReachedThisLevel;
}

[System.Serializable]
public struct EnemySet {
	[Range (0, 4)]
	public int themeColorIndex;
	[Range (0, 100)]
	public int requiredLevel;
	public ObjectList enemyListReference;
}

public class GameplayManager : MonoBehaviour {

	public static GameplayManager Instance;

	#region Public Variables

#if UNITY_EDITOR
	public bool isShowLevelInfo;
	public bool IsStartOnPlayInEditor;
#endif

    public bool scaleObjectiveOnDifferentDevice;

	[Space (5.0f)]
	[Header ("Gameplay HUD")]
	public TextMeshProUGUI scoreTextReference;
	public TextMeshProUGUI levelTextReference;
	public TextMeshProUGUI highScoreTextReference;

	[Space (5.0f)]
	[Header ("EnemySet")]
	public EnemySet[] enemySet;

	[Space (5.0f)]
	[Header ("Difficulty Configuretion")]
	public bool IsLevelResetOnEveryGame;
	[Space (5.0f)]
	[Tooltip ("if : value = 25 & difficulty level is 3. The following level will be reached on score 75")]
	[Range (1.0f, 10000.0f)]
	public long baseValueOfDifficultyLevel;
    [Tooltip("Level - 2 Will be reached 50, LVL - 3 = 100, Level 4 - 200")]
    [Range(1f,5f)]
    public float increasingValue;
    public bool increaseByPercentage;
	[Range (0.0f, 1.0f)]
	public float percentageToIncreaseByLevel;
    public int maxLimitOfObjective = 1000;

	public UnityEvent OnDifficultyLevelChange;
	public GameDifficultyLevel[] difficultyLevel;

	[Space (5.0f)]
	[Header ("Callback")]
	public UnityEvent OnGameStart;
	public UnityEvent OnGameOver;

	#endregion

	//----------
	#region Public Callback

	public void IncreaseLevel(){

		int t_CurrentLevel = PlayerPrefs.GetInt (LEVEL_RESET_REFERENCE);
		PlayerPrefs.SetInt (LEVEL_RESET_REFERENCE, ++t_CurrentLevel);
	}

	public void DecreaseLevel(){

		int t_CurrentLevel = PlayerPrefs.GetInt (LEVEL_RESET_REFERENCE);
		PlayerPrefs.SetInt (LEVEL_RESET_REFERENCE, --t_CurrentLevel);
	}

	public void ResetLevel(){

		PlayerPrefs.SetInt (LEVEL_RESET_REFERENCE, 0);
	}

	#endregion

	//----------
	#region Configuretion		:	Class

	void Awake () {

		Instance = this;
	}

	void Start () {

#if UNITY_EDITOR

		if (IsStartOnPlayInEditor) {
			PreProcess ();
		}

#endif

		InvokeRepeating ("InfoDisplay", 0, 1.0f);
		ResetStackableScore ();
		//GameThemeManager.Instance.ChangeColorTheme (GameplayManager.Instance.difficultyLevel[GameplayManager.Instance.GetCurrentDifficultyLevel ()].enemySet.themeColorIndex);
	}

	public void PreProcess () {

		OnGameStart.Invoke ();

		StartTrackingGameDifficultyLevel ();
	}

	public void RestoreToDefault () {

		StopTrackingGameDifficultyLevel ();
	}

	private void InfoDisplay () {

		//levelTextReference.text = "LEVEL " + (GetCurrentDifficultyLevel () + 1).ToString ();
		//highScoreTextReference.text = "HIGH SCORE " + MathFunction.Instance.GetCurrencyInFormat (GetHighScore ()).ToString ();
	}

	#endregion

	//--------
	#region Public Callback		:	Score & Objective System (Level Based)

	public void ResetScore () {

		PreProcessForScoringSystem ();
	}

	public void IncreaseScore (float t_Value) {

		m_CurrentScore += t_Value;
		IncreaseStackableScore (t_Value);
	}

	public float GetCurrentScore () {

		return m_CurrentScore;
	}

	public void ResetHighScore () {
		PlayerPrefs.SetInt (HIGH_SCORE_TRACKER_REFERENCE, 0);
	}

	public int GetHighScore () {
		return PlayerPrefs.GetInt (HIGH_SCORE_TRACKER_REFERENCE, 0);
	}

    public void ResetObjective() {

        PreProcessForObjectiveSystem();
    }

    public void IncreaseAccomplishmentOfObjective(float t_Value) {

        m_CurrentObjective += t_Value;
    }

    public void OverrideAccomplishmentOfObject(float t_Value) {

        m_CurrentObjective = t_Value;
    }

    public float GetNumberOfAccomplishedObjective() {

        return m_CurrentObjective;
    }

    public float GetAccomplishedObjectiveAsProgression() {

        return m_CurrentObjective / GetRequiredObjectiveToReachNextDifficultyLevel();
    }

	#endregion

	//--------
	#region Public Callback		:	Score System (Stacked)

	private float m_CurrentStackableScore;

	public void ResetStackableScore () {

		m_CurrentStackableScore = 0f;
	}

	public float GetCurrentStackableScore () {
		return m_CurrentStackableScore;
	}

	public void IncreaseStackableScore (float t_Value) {
		m_CurrentStackableScore += t_Value;
	}

    #endregion

    //--------
    #region Configuretion		:	Scoring System

    private float m_CurrentObjective;
	private float m_CurrentScore;
	[HideInInspector]
	public string LEVEL_RESET_REFERENCE = "LEVEL_RESET_REFERENCE";
	private string HIGH_SCORE_TRACKER_REFERENCE = "HIGH_SCORE_TRACKER_REFERENCE";
	private bool[] m_IsDifficultyLevelReached;

    private void PreProcessForObjectiveSystem() {

        m_CurrentObjective = 0;
    }

	private void PreProcessForScoringSystem () {

		//scoreTextReference.text = "0";

		m_CurrentScore = 0.0f;

		if (IsLevelResetOnEveryGame) {

			PlayerPrefs.SetInt (LEVEL_RESET_REFERENCE, 0);
		}

		m_CurrentDifficultyLevel = 0;

		m_IsDifficultyLevelReached = new bool[difficultyLevel.Length];

	}

	public void CheckHighScore () {

		int t_CurrentHighScore = PlayerPrefs.GetInt (HIGH_SCORE_TRACKER_REFERENCE, 0);
		if (t_CurrentHighScore < m_CurrentStackableScore) {
			PlayerPrefs.SetInt (HIGH_SCORE_TRACKER_REFERENCE, Mathf.RoundToInt (m_CurrentStackableScore));
		}
	}

	#endregion

	//--------
	#region Public Callback		:	Difficulty Level Tracker

	private int m_CurrentDifficultyLevel;
	private bool m_IsTrackingGameDifficultyLevel;

	public int GetMaxDifficultyLevel () {

		return difficultyLevel.Length;
	}

	public int GetCurrentDifficultyLevel () {

		if (IsLevelResetOnEveryGame) {

			return m_CurrentDifficultyLevel;
		} else {

			return PlayerPrefs.GetInt (LEVEL_RESET_REFERENCE, 0);
		}
	}

	public float GetDifficultyComplexityRatio () {

		return (GetCurrentDifficultyLevel () / (GetMaxDifficultyLevel () * 1.0f));
	}

	public float GetRequiredObjectiveToReachNextDifficultyLevel () {

        if (!IsLevelResetOnEveryGame)
        {

            m_CurrentDifficultyLevel = PlayerPrefs.GetInt(LEVEL_RESET_REFERENCE, 0);
        }

		int t_NextDifficultyLevelIndex = m_CurrentDifficultyLevel;

        if (scaleObjectiveOnDifferentDevice)
        {
            if (DeviceInfoManager.Instance.IsDevice_iPad())
            {

                return difficultyLevel[t_NextDifficultyLevelIndex].requiredObjectiveToReachedThisLevel * 1.5f;
            }
            else if (DeviceInfoManager.Instance.IsDevice_iPhoneX())
            {

                return difficultyLevel[t_NextDifficultyLevelIndex].requiredObjectiveToReachedThisLevel * 0.9f;
            }
            else
            {

                return difficultyLevel[t_NextDifficultyLevelIndex].requiredObjectiveToReachedThisLevel;
            }
        }
        else {

            return difficultyLevel[t_NextDifficultyLevelIndex].requiredObjectiveToReachedThisLevel;
        }
        
	}

	public void StartTrackingGameDifficultyLevel () {

        PreProcessForObjectiveSystem();
        PreProcessForScoringSystem ();

		m_IsTrackingGameDifficultyLevel = true;

		StartCoroutine (DifficultyLevelTrackingController ());
	}

	public void StopTrackingGameDifficultyLevel () {

		m_IsTrackingGameDifficultyLevel = false;
	}

	#endregion

	//--------
	#region Configuretion		:	Difficulty Level Tracker

	/// <summary>
	/// GameBased Code
	/// </summary>
	/// <param name="t_CurrentObjective"></param>
	/// <param name="t_AbsoluteDifficultyLevel"></param>
	/// <returns></returns>
	private bool IsEndOfLevel(float  t_CurrentObjective, float t_AbsoluteDifficultyLevel){

		if(t_CurrentObjective >= t_AbsoluteDifficultyLevel){
			return true;
		}else{

			float t_Progression = t_CurrentObjective / t_AbsoluteDifficultyLevel;

			if(t_Progression >= 0.99f)
				return true;
			else
				return false;
		}
	}

	private IEnumerator DifficultyLevelTrackingController () {

		WaitUntil t_GamePauseAndResumeState = new WaitUntil (() => {

			if (!GameManager.Instance.IsGamePaused ()) {

				return true;
			} else {

				return false;
			}
		});

		WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame ();

		while (m_IsTrackingGameDifficultyLevel) {

			yield return t_GamePauseAndResumeState;
			yield return t_CycleDelay;

			//scoreTextReference.text = MathFunction.Instance.GetCurrencyInFormat (Mathf.FloorToInt (m_CurrentStackableScore));

			if (IsLevelResetOnEveryGame) {

				for (int difficultyLevelIndex =  0; difficultyLevelIndex < m_IsDifficultyLevelReached.Length; difficultyLevelIndex++) {

					if (!m_IsDifficultyLevelReached[difficultyLevelIndex]) {
                        // if : Difficulty Level Haven't Reached Yet

                        float t_AbsoluteDifficultyLevel = difficultyLevel[difficultyLevelIndex].requiredObjectiveToReachedThisLevel;

						if (IsEndOfLevel(m_CurrentObjective,t_AbsoluteDifficultyLevel)) {
							// if : It score reached the required bar
                            
							OnDifficultyLevelChange.Invoke ();
							difficultyLevel[difficultyLevelIndex].OnDifficultyLevelReached.Invoke ();
							m_IsDifficultyLevelReached[difficultyLevelIndex] = true;

							m_CurrentDifficultyLevel = difficultyLevelIndex;

							CheckHighScore ();
							ResetNumberOfFailureInSameLevel ();
                            
							Debug.Log ("Difficulty Level Reached : (" + difficultyLevelIndex + ")");
						}

						break;
					}
				}
			} else {

				int t_DifficultyLevelIndex = PlayerPrefs.GetInt (LEVEL_RESET_REFERENCE, 0);

				float t_AbsoluteDifficultyLevel = GetRequiredObjectiveToReachNextDifficultyLevel();
				
				if (IsEndOfLevel(m_CurrentObjective, t_AbsoluteDifficultyLevel)) {

					t_DifficultyLevelIndex++;

					OnDifficultyLevelChange.Invoke ();
					PlayerPrefs.SetInt (LEVEL_RESET_REFERENCE, t_DifficultyLevelIndex);
					m_CurrentDifficultyLevel = t_DifficultyLevelIndex;
					//Debug.Log("### Called Color Theme : (Our Method)");
					//GameThemeManager.Instance.ChangeColorTheme (GameplayManager.Instance.difficultyLevel[m_CurrentDifficultyLevel].enemySet.themeColorIndex);

					CheckHighScore ();
					ResetNumberOfFailureInSameLevel ();

					//GameplayController.Instance.EndGame (true);

                    //FacebookAnalyticsManager.Instance.FBALevelComplete(t_DifficultyLevelIndex);

					Debug.Log ("Difficulty Level Reached : (" + t_DifficultyLevelIndex + ")");
				}
			}
		}
	}

	#endregion

	//--------
	#region Configuretion		:	Stage Difficulty Tracker

	private string ID_FOR_COUNTING_FAILURE_FOR_SAME_LEVEL = "ID_FOR_COUNTING_FAILURE_FOR_SAME_LEVEL";

	private void IncrementNumberOfFailureInSameLevel () {

		PlayerPrefs.SetInt (ID_FOR_COUNTING_FAILURE_FOR_SAME_LEVEL, GetNumberOfFailInSameLevel () + 1);
	}

	private void ResetNumberOfFailureInSameLevel () {
		PlayerPrefs.SetInt (ID_FOR_COUNTING_FAILURE_FOR_SAME_LEVEL, 0);
	}

	private int GetNumberOfFailInSameLevel () {
		return PlayerPrefs.GetInt (ID_FOR_COUNTING_FAILURE_FOR_SAME_LEVEL, 0);
	}
	public void ConfigOfSameLevelFailure () {

		int t_FailBoudary = Mathf.CeilToInt (GetCurrentDifficultyLevel () / 10.0f);
		if (GetNumberOfFailInSameLevel () >= t_FailBoudary) {

			ResetNumberOfFailureInSameLevel ();

			int t_NumberOfEnemySet = enemySet.Length;
			List<EnemySet> t_EnemySet = new List<EnemySet> ();
			for (int enemySetIndex = 0; enemySetIndex < t_NumberOfEnemySet; enemySetIndex++) {

				if (enemySet[enemySetIndex].requiredLevel <= GetCurrentDifficultyLevel ()) {
					t_EnemySet.Add (enemySet[enemySetIndex]);
				}
			}

			if (t_EnemySet.Count > 1) {
				while (true) {

					EnemySet t_NewEnemySet = t_EnemySet[Random.Range (0, t_EnemySet.Count)];
					if (t_NewEnemySet.themeColorIndex != difficultyLevel[GetCurrentDifficultyLevel ()].enemySet.themeColorIndex) {

						difficultyLevel[GetCurrentDifficultyLevel ()].enemySet.themeColorIndex = t_NewEnemySet.themeColorIndex;
						difficultyLevel[GetCurrentDifficultyLevel ()].enemySet.enemyListReference = t_NewEnemySet.enemyListReference;
						//Debug.Log("### Called Color Theme : (Reset For Failure)");
						break;
					}
				}
			}
		} else {

			IncrementNumberOfFailureInSameLevel ();
		}
	}

	#endregion
}