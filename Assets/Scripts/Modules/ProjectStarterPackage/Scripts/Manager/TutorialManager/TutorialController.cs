﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TutorialController : MonoBehaviour
{
    #region Public Variables

    public TutorialManager tutorialManagerReference;
    public GameObject tutorialCanvasReference;

    [Space(5)]
    public Button       buttonReference;
    public Animator     buttonAnimatorReference;

    public Image[] letterGridReferences;
    public TextMeshProUGUI[] letterTMPROReferences;
    public Transform[] gridTransformReference;
    #endregion

    //---------------------------------
    #region Private Variables

    private string IS_SHOWN_TUTORIAL_FOR_FIRST_TIME = "IS_SHOWN_TUTORIAL_FOR_FIRST_TIME";

    private Color[] m_InitialColor;
    private string[] m_LetterText;
    private Vector2[] m_InitialPosition;
    private Vector2[] m_InitialScale;

    #endregion

    // Start is called before the first frame update
    void Start()
    {

        m_InitialColor = new Color[letterGridReferences.Length];
        for (int index = 0; index < m_InitialColor.Length; index++)
        {
            m_InitialColor[index] = letterGridReferences[index].color;
        }

        m_LetterText = new string[letterTMPROReferences.Length];
        for (int index = 0; index < m_LetterText.Length; index++) {
            m_LetterText[index] = letterTMPROReferences[index].text;
        }

        m_InitialPosition = new Vector2[gridTransformReference.Length];
        m_InitialScale = new Vector2[gridTransformReference.Length];
        for (int index = 0; index < m_InitialPosition.Length; index++)
        {
            m_InitialPosition[index] = gridTransformReference[index].position;
            m_InitialScale[index] = gridTransformReference[index].localScale;
        }

        if (PlayerPrefs.GetInt(IS_SHOWN_TUTORIAL_FOR_FIRST_TIME, 0) == 0) {

            ShowTutorial();
            PlayerPrefs.SetInt(IS_SHOWN_TUTORIAL_FOR_FIRST_TIME, 1);
        }

        buttonReference.onClick.AddListener(delegate
        {
            ShowTutorial();
        });
    }

    //---------------------------------
    #region Public Callback

    public void ShowTutorialButton() {
        
        buttonAnimatorReference.SetTrigger("APPEAR");
    }

    public void HideTutorialButton() {

        buttonAnimatorReference.SetTrigger("DISAPPEAR");
    }

    public void ShowTutorial() {

        for (int index = 0; index < m_InitialColor.Length; index++)
        {
            letterGridReferences[index].color = m_InitialColor[index];
        }

        for (int index = 0; index < m_LetterText.Length; index++)
        {
            letterTMPROReferences[index].text = m_LetterText[index];
        }

        for (int index = 0; index < m_InitialPosition.Length; index++)
        {
            gridTransformReference[index].position = m_InitialPosition[index];
            gridTransformReference[index].localScale = m_InitialScale[index];
        }

        if (!tutorialCanvasReference.activeInHierarchy)
            tutorialCanvasReference.SetActive(true);

        tutorialManagerReference.ResetAllTutorial();
        tutorialManagerReference.StartTutorial(0);
    }

    public void ResetInitialTutorial() {

        tutorialManagerReference.ResetAllTutorial();
        PlayerPrefs.SetInt(IS_SHOWN_TUTORIAL_FOR_FIRST_TIME, 0);
    }

    #endregion
}
