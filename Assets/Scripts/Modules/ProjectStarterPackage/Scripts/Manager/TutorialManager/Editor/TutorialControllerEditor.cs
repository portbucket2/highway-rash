﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TutorialController))]
public class TutorialControllerEditor : Editor
{
    private TutorialController Reference;

    private void OnEnable()
    {
        Reference = (TutorialController)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        CustomGUI();

        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

    private void CustomGUI() {

        if (GUILayout.Button("Reset Initial Tutorial")) {

            Reference.ResetInitialTutorial();
        }
    }
}
