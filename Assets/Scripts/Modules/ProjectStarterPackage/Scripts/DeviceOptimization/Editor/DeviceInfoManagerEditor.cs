﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DeviceInfoManager))]
public class DeviceInfoManagerEditor : Editor {

	private DeviceInfoManager Reference;

	/// <summary>
	/// This function is called when the object becomes enabled and active.
	/// </summary>
	void OnEnable()
	{
		Reference = (DeviceInfoManager) target;
		if(DeviceInfoManager.Instance == null){
			DeviceInfoManager.Instance = Reference;
		}	
	}
}
