﻿using UnityEngine;

public class CrossPlatformUI : MonoBehaviour
{
    
    #region Public Variables

    public static CrossPlatformUI Instance;

    public CrossPlatformSpriteBundle dataContainerOfSpriteBundle;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
        }
    }

    #endregion

    #region Configuretion

    private bool IsValidSpriteBundleId(int t_SpriteBundleId) {

        if (t_SpriteBundleId >= 0 && t_SpriteBundleId < dataContainerOfSpriteBundle.spriteBundle.Length)
        {
            return true;
        }

        Debug.LogError("Invalid SpriteBundleId : " + t_SpriteBundleId);
        return false;
    }

    private Sprite GetSpriteBundle(int t_SpriteBundleId) {

        if (DeviceInfoManager.Instance.IsDevice_iPhone())
        {
            return dataContainerOfSpriteBundle.spriteBundle[t_SpriteBundleId].iPhone8;
        }
        else if (DeviceInfoManager.Instance.IsDevice_iPhoneX()) {
            return dataContainerOfSpriteBundle.spriteBundle[t_SpriteBundleId].iPhoneXSMax;
        }
        else if (DeviceInfoManager.Instance.IsDevice_iPad())
        {
            return dataContainerOfSpriteBundle.spriteBundle[t_SpriteBundleId].iPadPro;
        }

        Debug.LogError("Not an apple device");
        return dataContainerOfSpriteBundle.spriteBundle[t_SpriteBundleId].iPhone8;
    }

    #endregion

    #region Public Callback

    public Sprite GetSprite(string t_SpriteBundleId) {

        int t_NumberOfSpriteBundle = dataContainerOfSpriteBundle.spriteBundle.Length;
        for (int bundleIndex = 0; bundleIndex < t_NumberOfSpriteBundle; bundleIndex++) {
            if (dataContainerOfSpriteBundle.spriteBundle[bundleIndex].bundleName == t_SpriteBundleId) {
                return GetSpriteBundle(bundleIndex);
            }
        }

        Debug.LogError("Invalid SpriteBundleId : " + t_SpriteBundleId);
        return null;
    }

    public Sprite GetSprite(int t_SpriteBundleId)
    {
        if (IsValidSpriteBundleId(t_SpriteBundleId)) {

            return GetSpriteBundle(t_SpriteBundleId);
        }

        return null;
    }

    #endregion
}
