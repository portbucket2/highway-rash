﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetBarrier : MonoBehaviour
{
    public List<NPCCharacter> npcCharacterList;
    
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            for (int i = 0; i < npcCharacterList.Count; i++)
            {
                npcCharacterList[i].PlayerComeAtBarrier(other.transform);
            }
        }
    }
}
