﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatonTrigger : MonoBehaviour
{
    public KickSide kickSide;
    
    private void OnTriggerEnter(Collider other)
    {
        
        Debug.Log(other.name + " BATON");
        if (other.CompareTag("NPCCharacter"))
        {
            PlayerController.Instance.Triggerred(kickSide, other.transform);
        }
    
        if (other.CompareTag("NPCBike"))
        {
            PlayerController.Instance.TriggeredNPCBike(kickSide, other.transform);
        }
    
    }
}
