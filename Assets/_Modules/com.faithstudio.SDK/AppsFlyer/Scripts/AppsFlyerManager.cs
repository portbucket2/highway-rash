﻿namespace com.faithstudio.SDK
{
	using UnityEngine;

	public class AppsFlyerManager : MonoBehaviour
	{

		public static AppsFlyerManager Instance;

		#region Public Variables

		public string appDevKey;

		[Space(5.0f)]
		public string bundleIdForAndroid;
		public string bundleIdForiOS;

		#endregion

		//----------------------------------------
		#region Private Variables

		#endregion

		//----------------------------------------
		#region Mono Behaviour

		private void Awake()
		{
			if (Instance == null)
			{

				Instance = this;
				DontDestroyOnLoad(gameObject);
			}
			else
			{

				Destroy(gameObject);
			}
		}

		private void Start()
		{

			/* Mandatory - set your AppsFlyer’s Developer key. */
			AppsFlyer.setAppsFlyerKey(appDevKey);
			AppsFlyer.setIsDebug(true);


#if UNITY_IOS
  /* Mandatory - set your apple app ID
   NOTE: You should enter the number only and not the "ID" prefix */
    AppsFlyer.setAppID(bundleIdForiOS);
        AppsFlyer.getConversionData();
        AppsFlyer.trackAppLaunch();
#elif UNITY_ANDROID
        /* Mandatory - set your Android package name */
        AppsFlyer.setAppID(bundleIdForAndroid);
            /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
            AppsFlyer.init(appDevKey, "AppsFlyerTrackerCallbacks");
#endif

		}

		#endregion

		//----------------------------------------
		#region Configuretion

		#endregion

		//----------------------------------------
		#region Public Callback

		#endregion
	}
}


